$('#history').on('show.bs.collapse', function () {
    $("#history p").addClass("animate__animated animate__fadeInRight");
})

$('#workExperience').on('show.bs.collapse', function () {
    $("#workExperience p").addClass("animate__animated animate__fadeInLeft");
})

$('#education').on('show.bs.collapse', function () {
    $("#education p, #education li").addClass("animate__animated animate__fadeInRight");
})

$('#skills').on('show.bs.collapse', function () {
    $("#skills p").addClass("animate__animated animate__fadeInLeft");
})

$('#interests').on('show.bs.collapse', function () {
    $("#interests p").addClass("animate__animated animate__fadeInRight");
})

$('#myFoto').on('mouseenter', function () {
    $("#myFoto").addClass("animate__animated animate__pulse");
})

$('#myFoto').on('mouseleave', function () {
    $("#myFoto").removeClass("animate__animated animate__pulse");
})

//To jest inna metoda na osiągnięcie #myFoto

// $('#myFoto').hover(function () {
//     $("#myFoto").addClass("animate__animated animate__shakeY");
// }, function () {
//     $("#myFoto").removeClass("animate__animated animate__shakeY");
// })

$('#workExperienceFoto').on('mouseenter', function () {
    $("#workExperienceFoto").addClass("animate__animated animate__swing");
})

$('#workExperienceFoto').on('mouseleave', function () {
    $("#workExperienceFoto").removeClass("animate__animated animate__swing");
})

$('#educationFoto').on('mouseenter', function () {
    $("#educationFoto").addClass("animate__animated animate__wobble");
})

$('#educationFoto').on('mouseleave', function () {
    $("#educationFoto").removeClass("animate__animated animate__wobble");
})

$('#skillsFoto').on('mouseenter', function () {
    $("#skillsFoto").addClass("animate__animated animate__rotateIn");
})

$('#skillsFoto').on('mouseleave', function () {
    $("#skillsFoto").removeClass("animate__animated animate__rotateIn");
})

$('#interestsFoto').on('mouseenter', function () {
    $("#interestsFoto").addClass("animate__animated animate__jello");
})

$('#interestsFoto').on('mouseleave', function () {
    $("#interestsFoto").removeClass("animate__animated animate__jello");
})

var galTable = new Array();
var galx = 0;

function simplecookienotification_v01_create_cookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
    document.getElementById("simplecookienotification_v01").style.display = "none";
}

function simplecookienotification_v01_read_cookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(";");
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == " ") c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}
var simplecookienotification_v01_jest = simplecookienotification_v01_read_cookie(
"simplecookienotification_v01");
if (simplecookienotification_v01_jest == 1) {
    document.getElementById("simplecookienotification_v01").style.display = "none";
}